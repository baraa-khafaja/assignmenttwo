package com.example.student;


import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.student.R;
import com.example.student.studentFile;

import java.util.List;

public class studentAdapter extends RecyclerView.Adapter<studentAdapter.Student> {

    Context context ;
    List<studentFile> studentFile;


    public studentAdapter(Context context ,  List<studentFile> StudentFile) {
        this.context = context;
        this.studentFile = StudentFile;
    }

    @NonNull
    @Override
    public Student onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view =LayoutInflater.from(context).inflate(R.layout.activity_mainstudent , parent , false);
        return new Student(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Student holder, int position) {

        holder.setData(studentFile.get(position));
    }

    @Override
    public int getItemCount() {
        return studentFile.size();
    }

    class Student extends RecyclerView.ViewHolder{
        TextView  name_std , id_std , avg_std , level_std;
        public Student(@NonNull View itemView) {
            super(itemView);
            name_std = itemView.findViewById(R.id.name_std);
            id_std = itemView.findViewById(R.id.id_std);
            avg_std = itemView.findViewById(R.id.avg_std);
            level_std = itemView.findViewById(R.id.level_std);
        }

        public void setData(final studentFile std) {
            name_std.setText(std.getName());
            id_std.setText(std.getIdStd());
            avg_std.setText(std.getAvarege());



            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(context,std.getName(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(itemView.getContext() ,MainActivity.class);
                    intent.putExtra("id",std.getIdStd());
                    intent.putExtra("name",std.getName());
                    intent.putExtra("avarage",std.getAvarege());
                    intent.putExtra("level",std.getLevel());
                    itemView.getContext().startActivity(intent);
                }
            });
        }
    }
}
