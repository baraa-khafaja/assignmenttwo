package com.example.student;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

class studentMainFile extends AppCompatActivity {
    RecyclerView studentList_rv;
    studentAdapter studentAdapter;
    List<com.example.student.studentFile> studentFile  = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainstudent);

        studentFile.add(new studentFile("Baraa ","20150701","%","5"));//
        studentFile.add(new studentFile("Rozana ","2016000","%","4"));//
        studentFile.add(new studentFile("Jenin","2442002","%","3"));//
        studentFile.add(new studentFile(
                "Hussam ","2312007","%","2"));//
        studentFile.add(new studentFile("Mohammad","1112011","%","4"));//
        //


        studentList_rv = findViewById(R.id.studentList_rv);
        studentList_rv.setLayoutManager(new LinearLayoutManager(this));
        studentAdapter = new studentAdapter(this ,studentFile);
        studentList_rv.setAdapter(studentAdapter);



    }
}
