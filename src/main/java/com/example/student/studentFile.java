package com.example.student;

public class studentFile {
    String name ;
    String IdStd;
    String Avarege;
    String Level;


    public studentFile(String name, String IdStd, String Avarege, String Level) {
        this.name = name;
        this.IdStd = IdStd;
        this.Avarege = Avarege;
        this.Level = Level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdStd() {
        return IdStd;
    }

    public void setIdStd(String IdStd) {
        this.IdStd = IdStd;
    }

    public String getAvarege() {
        return Avarege;
    }

    public void setAvarege(String Avarege) {
        this.Avarege = Avarege;
    }

    public String getLevel() {
        return Level;
    }

    public void setLevel(String Level) {
        this.Level = Level;
    }
}
